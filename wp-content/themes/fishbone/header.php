<!DOCTYPE html>
<html lang="pt-Br">

<head>

    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/assets/css/vendors.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/assets/css/main.css">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory');?>/assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php bloginfo('template_directory');?>/assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory');?>/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory');?>/assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory');?>/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_directory');?>/assets/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo('template_directory');?>/assets/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title><?php echo get_option("blogname"); ?></title>

    <?php wp_head(); ?>
</head>

<body class="preloader-visible" data-barba="wrapper">


    <!-- preloader start -->
    <div class="preloader js-preloader">
        <div class="preloader__bg"></div>

        <div class="preloader__progress">
            <div class="preloader__progress__inner"></div>
        </div>
    </div>
    <!-- preloader end -->


    <!-- cursor start -->
    <div class="cursor js-cursor">
        <div class="cursor__wrapper">
            <div class="cursor__follower js-follower"></div>
            <div class="cursor__label js-label"></div>
            <div class="cursor__icon js-icon"></div>
        </div>
    </div>
    <!-- cursor end -->


    <!-- barba container start -->
    <div class="barba-container" data-barba="container">

        <!-- to-top-button start -->
        <div data-cursor class="backButton js-backButton">
            <span class="backButton__bg"></span>
            <div class="backButton__icon__wrap">
                <i class="backButton__button js-top-button" data-feather="arrow-up"></i>
            </div>
        </div>
        <!-- to-top-button end -->


        <main class="">


            