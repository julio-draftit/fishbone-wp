 <!-- nav start -->
 <nav class="nav js-nav">
                    <div class="nav__inner js-nav-inner">
                        <div class="nav__bg js-nav-bg"></div>

                        <div class="nav__container">
                            <div class="nav__header">
                                <button type="button" class="nav-button-back js-nav-back">
                  <i class="icon" data-feather="arrow-left-circle"></i>
                </button>

                                <button type="button" class="nav-btn-close js-nav-close pointer-events-none">
                  <i class="icon" data-feather="x"></i>
                </button>
                            </div>

                            <div class="nav__content">
                                <div class="nav__content__left">
                                    <div class="navList__wrap">
                                        <ul class="navList js-navList">
                                            <li>
                                                <a data-barba href="index.html">
                                                  Home
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="nav__content__right">
                                    <div class="nav__info">
                                        <div class="nav__info__item js-navInfo-item">
                                            <h5 class="text-sm tracking-none fw-500">
                                                Endereço
                                            </h5>

                                            <div class="nav__info__content text-lg text-white mt-16">
                                                <p>
                                                Rua Oswaldo Collino, 294 - Pres. Altino<br/>Osasco - SP, 02675-031
                                                </p>
                                            </div>
                                        </div>

                                        <div class="nav__info__item js-navInfo-item">
                                            <h5 class="text-sm tracking-none fw-500">
                                                Siga-nos 
                                            </h5>

                                            <div class="nav__info__content text-lg text-white mt-16">
                                                <a href="https://www.facebook.com/agencia.fishbone">Facebook</a>
                                                <a href="https://www.instagram.com/agencia.fishbone/">Instagram</a>
                                                <a href="#">Linkedin</a>
                                                <a href="https://twitter.com/AgenciaFishbone">Twitter</a>
                                                <!-- <a href="https://wa.me/5511951294285?text=Ol%C3%A1,%20gostaria%20de%20um%20or%C3%A7amento">whatsApp</a> -->
                                            </div>
                                        </div>

                                        <div class="nav__info__item js-navInfo-item">
                                            <h5 class="text-sm tracking-none fw-500">
                                                Contato
                                            </h5>

                                            <div class="nav__info__content text-lg text-white mt-16">
                                                <a href="<?php echo get_option("siteurl"); ?>/contato/" class="btn btn-primary">Entre em contato</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>