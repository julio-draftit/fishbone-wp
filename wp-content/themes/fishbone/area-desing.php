<?php
/*
Template Name: Design
Template Post Type: areas
*/
?>

<?php get_header(); ?>

<style>
    .text-accent {
    color: #ff1d25;
}
    </style>
<header class="header -dark -sticky-light js-header-light js-header">
                <!-- header__bar start -->
                <div class="header__bar">
                    <div class="header__logo js-header-logo">
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__light js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png 1x, <?php bloginfo('template_directory');?>/assets/img/logo/logo-light-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png" alt="Logo">
                        </a>
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__dark js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png 1x, <?php bloginfo('template_directory');?>/assets/img/logo/logo-light-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png" alt="Logo">
                        </a>
                    </div>

                    <div class="header__menu js-header-menu">
                        <button type="button" class="nav-button-open js-nav-open">
              <i class="icon" data-feather="menu"></i>
            </button>
                    </div>
                </div>
                <!-- header__bar end -->

               <?php include('menu.php');?>
                <!-- nav end -->
            </header>
            <!-- header end -->

            <!-- page header start -->

<section class="masthead -type-work-1 -full-screen bg-black sm:text-center js-masthead-type-work-1 js-bg">
    <div class="masthead__bg overlay-black-md js-image" data-parallax="0.7">
        <div class="bg-image js-lazy js-bg-item" data-parallax-target="" data-bg="<?php the_post_thumbnail_url(); ?>"></div>
    </div>
    <div class="masthead__content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-sm-10 z-3">
                    <div data-split="lines">
                        <p class="masthead__subtitle text-lg sm:text-base text-white mb-32 sm:mb-24 js-subtitle"><?php the_excerpt();?></p>

                    </div>
                    <div data-split="lines">
                        <h1 class="masthead__title text-white fw-700 js-title"><?php the_title();?></h1>
                    </div>
                    <div class="masthead__info__wrap">
                        <div class="row y-gap-32">
                            <div class="col-lg-auto col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Role</h5>
                                        <p class="text-lg leading-sm text-light mt-12 sm:mt-8">Designer</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-auto offset-lg-1 col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Start</h5>
                                        <p class="text-lg leading-sm text-light mt-12 sm:mt-8">08 Jan 2020</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-auto offset-lg-1 col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Launch</h5>
                                        <p class="text-lg leading-sm text-light mt-12 sm:mt-8">15 Mar 2020</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-auto offset-lg-1 col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Website</h5>
                                        <a class="button d-block fw-500 text-lg leading-sm text-white mt-12 sm:mt-8" href="https://themeforest.net/">
                                        themeforest.net
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- page header end -->

 <!-- section start -->
 <section class="layout-pt-lg layout-pb-lg bg-dark-1">
        <!-- container start -->
        <div class="container">
          <!-- row start -->
          <div class="row justify-content-between">

            <div class="col-auto">
              <div class="d-flex text-xs tracking-md pt-4">
                <p class="text-accent fw-600 mr-16">01.-</p>
                <p class="text-light uppercase fw-500">Context</p>
              </div>
            </div>


            <div class="col-xl-7 col-lg-8">
              <div class="md:mt-20">
                <h2 class="text-3xl fw-600 text-white">
                  Challenge
                </h2>
                <p class="text-lg text-white mt-24 sm:mt-16">
                  Tree brought herb us abundantly thing our face herb. Make midst every dry moving. Moveth was tree. Won't waters of. Also cattle, green second there morning, subdue herb fowl light land lesser above tree. Night his air give green bearing stars waters hath. Cattle midst called open. Land for was.
                </p>
                <p class="leading-5xl text-light mt-24 sm:mt-16">
                  Upon that night. Without day earth fowl. Midst won't creature beginning fly wherein. Isn't. That to him spirit forth male Called land sixth made, them won't. Green face years thing beast. Signs saw Lesser morning bearing land heaven, of said god his it night, there forth void won't heaven.
                </p>
              </div>
            </div>

          </div>
          <!-- row end -->
        </div>
        <!-- container end -->
      </section>
      <!-- section end -->


      <!-- section start -->
      <div class="bg-dark-1">
        <div data-parallax="0.7" class="h-100vh">
          <div data-parallax-target class="bg-image js-lazy" data-bg="img/project2/wide-1.jpg"></div>
        </div>
      </div>
      <!-- section end -->


      <!-- section start -->
      <section class="layout-pt-lg layout-pb-lg bg-dark-1">
        <!-- container start -->
        <div class="container">
          <!-- row start -->
          <div class="row justify-content-between">
            <div class="col-xl-7 col-lg-8">
              <div class="d-flex text-xs tracking-md pt-4">
                <p class="text-accent fw-600 mr-16">02.-</p>
                <p class="uppercase fw-500 text-light">Goal</p>
              </div>

              <h2 class="text-3xl fw-600 text-white mt-20">
                Challenge
              </h2>
              <p class="text-lg text-white mt-24 sm:mt-16">
                Tree brought herb us abundantly thing our face herb. Make midst every dry moving. Moveth was tree. Won't waters of. Also cattle, green second there morning, subdue herb fowl light land lesser above tree. Night his air give green bearing stars waters hath. Cattle midst called open. Land for was.
              </p>
              <p class="leading-5xl text-light mt-24 sm:mt-16">
                Upon that night. Without day earth fowl. Midst won't creature beginning fly wherein. Isn't. That to him spirit forth male Called land sixth made, them won't. Green face years thing beast. Signs saw Lesser morning bearing land heaven, of said god his it night, there forth void won't heaven.
              </p>
            </div>
          </div>
          <!-- row end -->
        </div>
        <!-- container end -->
      </section>
      <!-- section end -->


      <!-- section start -->
      <div class="layout-pb-lg bg-dark-1">
        <!-- container start -->
        <div class="container">
          <div class="fancy-grid -col-2">

            <div class="fancy-grid__item">
              <a data-gallery="gallery1" class="glightbox" href="img/project2/long-1.jpg">
                <div data-parallax="0.8" class="ratio ratio-3:4">
                  <div data-parallax-target class="bg-image js-lazy" data-bg="img/project2/long-1.jpg"></div>
                </div>
              </a>
            </div>

            <div class="fancy-grid__item">
              <a data-gallery="gallery1" class="glightbox" href="img/project2/long-2.jpg">
                <div data-parallax="0.8" class="ratio ratio-3:4">
                  <div data-parallax-target class="bg-image js-lazy" data-bg="img/project2/long-2.jpg"></div>
                </div>
              </a>
            </div>

            <div class="fancy-grid__item">
              <a data-gallery="gallery1" class="glightbox" href="img/project2/long-3.jpg">
                <div data-parallax="0.8" class="ratio ratio-3:4">
                  <div data-parallax-target class="bg-image js-lazy" data-bg="img/project2/long-3.jpg"></div>
                </div>
              </a>
            </div>

            <div class="fancy-grid__item">
              <a data-gallery="gallery1" class="glightbox" href="img/project2/long-4.jpg">
                <div data-parallax="0.8" class="ratio ratio-3:4">
                  <div data-parallax-target class="bg-image js-lazy" data-bg="img/project2/long-4.jpg"></div>
                </div>
              </a>
            </div>

          </div>
        </div>
        <!-- container end -->
      </div>
      <!-- section end -->


      <!-- section start -->
      <section class="projects-nav">
        <!-- row start -->
        <div class="row no-gutters">

        <?php
		$prev_post = get_previous_post();
		if (!empty( $prev_post )){ ?>
          <div class="col-lg-6">
            <a data-barba href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>" class="projects-nav__item -prev bg-dark-1">
              <div class="projects-nav__img">
                <div class="bg-image js-lazy" data-bg="<?php $featured_img_url = get_the_post_thumbnail_url($prev_post->ID, 'full'); echo $featured_img_url;?>"></div>
              </div>

              <div class="projects-nav__content">
                <div class="projects-nav__icon">
                  <i class="size-xl str-width-xs text-white" data-feather="arrow-left-circle"></i>
                </div>

                <h3 class="text-4xl fw-400 tracking-none text-white">
                <?php echo esc_attr( $prev_post->post_title ); ?>
                </h3>

                <p class="projects-nav__text -prev text-sm fw-500 text-white tracking-md uppercase">
                  Anterior
                </p>
              </div>
            </a>
          </div>
          <?php } else { 	$args = array(
					'post_type' => 'areas',
					'posts_per_page' => 1,
				);
				$query = new WP_Query($args);
				remove_filter( 'the_excerpt', 'wpautop' );
				while($query->have_posts()) : $query->the_post();?>
            <div class="col-lg-6">
            <a data-barba href="<?php the_permalink();?>" class="projects-nav__item -prev bg-dark-1">
              <div class="projects-nav__img">
                <div class="bg-image js-lazy" data-bg="<?php the_post_thumbnail_url(); ?>"></div>
              </div>

              <div class="projects-nav__content">
                <div class="projects-nav__icon">
                  <i class="size-xl str-width-xs text-white" data-feather="arrow-left-circle"></i>
                </div>

                <h3 class="text-4xl fw-400 tracking-none text-white">
                <?php the_title();?>
                </h3>

                <p class="projects-nav__text -prev text-sm fw-500 text-white tracking-md uppercase">
                  Anterior
                </p>
              </div>
            </a>
          </div>
          <?php endwhile; wp_reset_query(); } ?>
          <?php
		$next_post = get_next_post();
		if (!empty( $next_post )){ ?>
          <div class="col-lg-6" style="background-color: #131419;">
            <a data-barba href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="projects-nav__item -next bg-dark-1">
              <div class="projects-nav__img">
                <div class="bg-image js-lazy" data-bg="<?php $featured_img_url = get_the_post_thumbnail_url($next_post->ID, 'full'); echo $featured_img_url;?>"></div>
              </div>

              <div class="projects-nav__content">
                <h3 class="text-4xl fw-400 tracking-none text-white">
                <?php echo esc_attr( $next_post->post_title ); ?>
                </h3>

                <div class="projects-nav__icon">
                  <i class="size-xl str-width-xs text-white" data-feather="arrow-right-circle"></i>
                </div>

                <p class="projects-nav__text -next text-sm fw-500 text-white tracking-md uppercase">
                  Próximo
                </p>
              </div>
            </a>
          </div>
          <?php } else { $args2 = array(
					'post_type' => 'areas',
					'posts_per_page' => 1,
                    'order' => 'ASC',
				);
				$query2 = new WP_Query($args2);
				remove_filter( 'the_excerpt', 'wpautop' );
				while($query2->have_posts()) : $query2->the_post();?>
                <div class="col-lg-6" style="background-color: #131419;">
                    <a data-barba href="<?php the_permalink();?>" class="projects-nav__item -next bg-dark-1">
                    <div class="projects-nav__img">
                        <div class="bg-image js-lazy" data-bg="<?php the_post_thumbnail_url(); ?>"></div>
                    </div>

                    <div class="projects-nav__content">
                        <h3 class="text-4xl fw-400 tracking-none text-white">
                        <?php the_title();?>
                        </h3>

                        <div class="projects-nav__icon">
                        <i class="size-xl str-width-xs text-white" data-feather="arrow-right-circle"></i>
                        </div>

                        <p class="projects-nav__text -next text-sm fw-500 text-white tracking-md uppercase">
                        Próximo
                        </p>
                    </div>
                    </a>
                </div>
            <?php endwhile; wp_reset_query(); } ?>
        </div>
        <!-- row end -->
      </section>
           

<?php get_footer(); ?>