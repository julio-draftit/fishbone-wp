<?php


function load_stylesheets(){
}

add_action('wp_enqueue_scripts', 'load_stylesheets');

function id_setup() {
    add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
    add_theme_support( 'title-tag' );
    
}
add_action( 'after_setup_theme', 'id_setup' );

function create_areas() {
    $supports = array(
        'title', // post title
        'editor', // post content
        'author', // post author
        'thumbnail', // featured images
        'excerpt', // post excerpt
        'custom-fields', // custom fields
        'comments', // post comments
        'revisions', // post revisions
        'post-formats', // post formats
        'page-attributes' 
        );
    register_post_type( 'Areas',
        // CPT Options
    array(
        'hierarchical' => false,
        'supports' => $supports,
        'labels' => array(
        'name' => __( 'Areas' ),
        'singular_name' => __( 'Areas' ),
        'attributes' => 'Page Attributes'
      ),
        'public' => true,
        'has_archive' => false,
        'show_in_rest' => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'capability_type'     => 'post',
        'menu_icon' => 'dashicons-slides',
        'rewrite' => array('slug' => 'Areas'),
     )
    );
}
    // Hooking up our function to theme setup
    add_action( 'init', 'create_areas' );

    
 


add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
  register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
}


    

function register_menu_footer() {
register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );
}
add_action( 'init', 'register_menu_footer' );

function register_menu_header() {
    register_nav_menu( 'header-menu', __( 'Header Menu' ) );
    }
    add_action( 'init', 'register_menu_header' );
    

function hide_admin_bar(){ return false; }
add_filter( 'show_admin_bar', 'hide_admin_bar' );
 



