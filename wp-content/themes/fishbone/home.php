<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>
<!-- header start -->
<header class="header -dark -sticky-light js-header-light js-header">
                <!-- header__bar start -->
                <div class="header__bar">
                    <div class="header__logo js-header-logo">
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__light js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png 1x, img/logo/logo-light-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png" alt="Logo">
                        </a>
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__dark js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-dark.png 1x, img/logo/logo-dark-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-dark.png" alt="Logo">
                        </a>
                    </div>

                    <div class="header__menu js-header-menu">
                        <button type="button" class="nav-button-open js-nav-open">
              <i class="icon" data-feather="menu"></i>
            </button>
                    </div>
                </div>
                <!-- header__bar end -->

               <?php include('menu.php');?>
                <!-- nav end -->
            </header>
            <!-- header end -->
<!-- section start -->
<section class="swiper-container sliderMain -type-2 js-sliderMain-type-2 js-slider-full-page">
                <!-- wrapper start -->
                <div class="swiper-wrapper">

                    <?php
                            
                        $args = array(
                            'post_type' => 'areas',
                            'order' => 'ASC',
                        );
                        $query = new WP_Query($args);
                        while($query->have_posts()) : $query->the_post();
                    ?>
                    <!-- swiper-slide start -->
                    <div class="swiper-slide">
                        <div class="slider__content text-center js-slider__content">
                            <div class="js-slider-title">
                                <a data-barba href="<?php the_permalink();?>">
                                    <h1 class="slider__title fw-800 text-white">
                                    <?php the_title();?>
                                    </h1>
                                </a>
                            </div>

                            <div class="slider__subtitle__inner overflow-hidden mt-32 js-slider-subtitle">
                                <p class="slider__subtitle text-white">
                                    <?php the_excerpt();?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- swiper-slide end -->
                    <?php endwhile; wp_reset_query(); ?>

                </div>
                <!-- wrapper end -->

                <!-- images start -->
                <div class="slider__images">

                    <?php
                            
                        $args = array(
                            'post_type' => 'areas',
                            'order' => 'ASC',
                        );
                        $query = new WP_Query($args);
                        while($query->have_posts()) : $query->the_post();
                    ?>
                    <div class="slider__img js-slider-img">
                        <div class="bg-image js-lazy" data-bg="<?php the_post_thumbnail_url(); ?>"></div>
                    </div>
                    <?php endwhile; wp_reset_query(); ?>

                </div>
                <!-- images end -->

                <!-- ui-element start -->
                <?php include('social.php');?>
                <!-- ui-element end -->

                <!-- ui-element start -->
                <div class="ui-element -bottom js-ui">
                    <div class="pagination -light js-pagination"></div>
                </div>
                <!-- ui-element end -->

                <!-- ui-element start -->
                <div class="ui-element -bottom-right sm:d-none js-ui">
                    <div class="navButton js-slider-main-nav">
                        <a type="button" class="navButton__item button -outline-white js-nav-prev text-white">
                            <i class="icon" data-feather="arrow-left"></i>
                        </a>

                        <button type="button" class="navButton__item button -outline-white js-nav-next text-white ml-20">
                            <i class="icon" data-feather="arrow-right"></i>
                        </button>
                    </div>
                </div>
                <!-- ui-element end -->
            </section>
            <!-- section end -->

            <?php get_footer(); ?>