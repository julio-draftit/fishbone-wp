<?php get_header(); ?>


<header class="header -dark -sticky-light js-header-light js-header">
                <!-- header__bar start -->
                <div class="header__bar">
                    <div class="header__logo js-header-logo">
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__light js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png 1x, img/logo/logo-light-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png" alt="Logo">
                        </a>
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__dark js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-dark.png 1x, img/logo/logo-dark-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-dark.png" alt="Logo">
                        </a>
                    </div>

                    <div class="header__menu js-header-menu">
                        <button type="button" class="nav-button-open js-nav-open">
              <i class="icon" data-feather="menu"></i>
            </button>
                    </div>
                </div>
                <!-- header__bar end -->

               <?php include('menu.php');?>
                <!-- nav end -->
            </header>
            <!-- header end -->

            <!-- page header start -->

<section class="masthead -type-work-1 -full-screen bg-black sm:text-center js-masthead-type-work-1 js-bg">
    <div class="masthead__bg overlay-black-md js-image" data-parallax="0.7">
        <div class="bg-image js-lazy js-bg-item" data-parallax-target="" data-bg="<?php the_post_thumbnail_url(); ?>"></div>
    </div>
    <div class="masthead__content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-sm-10 z-3">
                    <div data-split="lines">
                        <p class="masthead__subtitle text-lg sm:text-base text-white mb-32 sm:mb-24 js-subtitle"><?php the_excerpt();?></p>

                    </div>
                    <div data-split="lines">
                        <h1 class="masthead__title text-white fw-700 js-title"><?php the_title();?></h1>
                    </div>
                    <div class="masthead__info__wrap">
                        <div class="row y-gap-32">
                            <div class="col-lg-auto col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Role</h5>
                                        <p class="text-lg leading-sm text-light mt-12 sm:mt-8">Designer</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-auto offset-lg-1 col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Start</h5>
                                        <p class="text-lg leading-sm text-light mt-12 sm:mt-8">08 Jan 2020</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-auto offset-lg-1 col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Launch</h5>
                                        <p class="text-lg leading-sm text-light mt-12 sm:mt-8">15 Mar 2020</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-auto offset-lg-1 col-sm-6">
                                <div class="masthead__info">
                                    <div class="masthead__item js-info-item" data-split="lines">
                                        <h5 class="text-lg leading-sm fw-600 text-white">Website</h5>
                                        <a class="button d-block fw-500 text-lg leading-sm text-white mt-12 sm:mt-8" href="https://themeforest.net/">
                                        themeforest.net
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- page header end -->


            <?php $content = apply_filters('the_content',$post->post_content); echo  $content;?>

<?php get_footer(); ?>