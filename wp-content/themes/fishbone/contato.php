<?php
/*
Template Name: Contato
*/
?>
<?php get_header(); ?>

<!-- header start -->
<header class="header -dark -sticky-dark js-header-dark js-header">
                <!-- header__bar start -->
                <div class="header__bar">
                    <div class="header__logo js-header-logo">
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__light js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png 1x, img/logo/logo-light-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png" alt="Logo">
                        </a>
                        <a data-barba href="<?php echo get_option("siteurl"); ?>">
                            <img class="header__logo__dark js-lazy" data-srcset="<?php bloginfo('template_directory');?>/assets/img/logo/logo-dark.png 1x, img/logo/logo-dark-x2.png 2x" data-src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-dark.png" alt="Logo">
                        </a>
                    </div>

                    <div class="header__menu js-header-menu">
                        <button type="button" class="nav-button-open js-nav-open">
              <i class="icon" data-feather="menu"></i>
            </button>
                    </div>
                </div>
                <!-- header__bar end -->

               <?php include('menu.php');?>
                <!-- nav end -->
            </header>
            <!-- header end -->
<!-- section start -->
<section class="layout-pt-xl layout-pb-xs bg-dark-1">
        <!-- container start -->
        <div data-anim-wrap class="container">

          <!-- row start -->
          <div class="row">
            <div class="col-xl-9 offset-xl-1 col-lg-11">
              <div data-anim-child="slide-up delay-1" class="sectionHeading -lg">
                <p class="sectionHeading__subtitle text-white">
                CONTRATE-NOS
                </p>
                <h1 class="sectionHeading__title leading-sm text-white">
                Tem um projeto para discutir?<br/>Entrar em contato.
                </h1>
              </div>
            </div>
          </div>
          <!-- row end -->

          <!-- row start -->
          <div data-anim-child="slide-up delay-2" class="row justify-content-center layout-pt-md">
            <div class="col-xl-10">
              <div class="row x-gap-48 y-gap-48">
                <div class="col-lg-3 col-md-6 col-sm-8">
                  <h4 class="text-xl fw-600 text-white">
                    Endereço
                  </h4>
                  <div class="text-dark mt-12 text-white">
                    <p>Rua Oswaldo Collino, 294 - Pres. Altino<br/>Osasco - SP, 02675-031</p>
                  </div>
                </div>

                <div class="col-lg-auto offset-lg-1 col-md-6">
                  <h4 class="text-xl fw-600 text-white">
                    E-mail & Telefone
                  </h4>
                  <div class="text-dark mt-12">
                    <div>
                      <a class="button -underline text-white" href="mailto:contato@fishbone.marketing">contato@fishbone.marketing</a>
                    </div>
                    <div class="mt-4">
                      <a class="button -underline text-white" href="tel:+5511951294285">(11) 95129-4285</a>
                    </div>
                  </div>
                </div>

                <div class="col-md-auto offset-lg-1">
                  <h4 class="text-xl fw-600 text-white">
                    Siga-nos
                  </h4>
                  <div class="social -bordered mt-16 md:mt-12">

                    <a class="social__item text-white border-light" href="https://twitter.com/AgenciaFishbone">
                      <i class="fa fa-twitter"></i>
                    </a>

                    <a class="social__item text-white border-light" href="https://www.instagram.com/agencia.fishbone/">
                      <i class="fa fa-instagram"></i>
                    </a>

                    <a class="social__item text-white border-light" href="https://www.facebook.com/agencia.fishbone">
                      <i class="fa fa-facebook"></i>
                    </a>

                    <a class="social__item text-white border-light" href="#">
                      <i class="fa fa-linkedin"></i>
                    </a>
                    <a class="social__item text-white border-light" href="https://wa.me/5511951294285?text=Ol%C3%A1,%20gostaria%20de%20um%20or%C3%A7amento">
                      <i class="fa fa-whatsapp"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- row end -->

        </div>
        <!-- container end -->
      </section>
      <!-- section end -->


      <!-- section start -->
      <section class="layout-pt-md layout-pb-lg bg-dark-1">
        <!-- container start -->
        <div class="container">
          <!-- row start -->
          <div data-anim="slide-up delay-3" class="row justify-content-center">
            <div class="col-xl-10">
              <div class="sectionHeading -sm">
                <h2 class="sectionHeading__title text-white">
                Deixe-nos uma mensagem
                </h2>
                <p class="text-white leading-md mt-12">
                Use o formulário abaixo ou 
                  <a href="#" class="fw-700 text-white">envie-nos um e-mail</a>.
                </p>
              </div>
            </div>

            <div class="w-1/1"></div>

            <div class="col-xl-10 mt-48 sm:mt-32">
              <div class="contact-form -type-1">
                <form class="row x-gap-40 y-gap-32 js-ajax-form" method="POST" data-message-success="Your message has been sent! We will reply you as soon as possible." data-message-error="Something went wrong. Please contact us directly at <a href='hello@stukram.com'>hello@stukram.com</a>.">
                  <div class="col-lg-6">
                    <label class="js-input-group text-white">
                      Nome
                      <input type="text" name="name" data-required placeholder="Primeiro Nome">
                      <span class="form__error"></span>
                    </label>
                  </div>

                  <div class="col-lg-6">
                    <label class="js-input-group text-white">
                      Telefone (opicional)
                      <input type="text" name="phone" placeholder="(XX) XXXX-XXXX">
                      <span class="form__error"></span>
                    </label>
                  </div>

                  <div class="col-lg-6">
                    <label class="js-input-group text-white">
                      E-mail
                      <input type="text" name="email" data-required placeholder="nome@dominio.com.br">
                      <span class="form__error"></span>
                    </label>
                  </div>

                  <div class="col-lg-6">
                    <label class="js-input-group text-white">
                      Assunto
                      <input type="text" name="subject" placeholder="O que você está procurando?">
                      <span class="form__error"></span>
                    </label>
                  </div>

                  <div class="col-12">
                    <label class="js-input-group text-white">
                      Mensagem
                      <textarea name="message" rows="2" placeholder="Preencha sua mensagem"></textarea>
                      <span class="form__error"></span>
                    </label>
                  </div>

                  <div class="col-12 ajax-form-alert js-ajax-form-alert">
                    <div class="ajax-form-alert__content">
                    </div>
                  </div>

                  <div class="col-12">
                    <button class="button -md -white text-black">
                    Enviar mensagem
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- row end -->
        </div>
        <!-- container end -->
      </section>
      <!-- section end -->

<?php get_footer(); ?>