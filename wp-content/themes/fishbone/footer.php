<footer class="footer -type-1 bg-dark-1">
        <!-- container start -->
        <div class="container">

          <div class="footer__top">
            <!-- row start -->
            <div class="row y-gap-48 justify-content-between">
              <div class="col-lg-auto col-sm-12">
                <a href="home-1.html" class="footer__logo text-white">
                  <img src="<?php bloginfo('template_directory');?>/assets/img/logo/logo-light.png" />
                </a>
                
              </div>
              <div class="col-lg-auto col-sm-4">
              <h4 class="text-xl fw-500 text-white">
                  Siga-nos
                </h4>

                <div class="social -bordered mt-16 sm:mt-12">
                <a class="social__item text-white border-light" href="https://www.instagram.com/agencia.fishbone/">
                    <i class="fa fa-instagram"></i>
                  </a>
                  <a class="social__item text-white border-light" href="https://www.facebook.com/agencia.fishbone">
                    <i class="fa fa-facebook"></i>
                  </a>
                  <a class="social__item text-white border-light" href="https://twitter.com/AgenciaFishbone">
                    <i class="fa fa-twitter"></i>
                  </a>
                  <a class="social__item text-white border-light" href="#">
                    <i class="fa fa-linkedin"></i>
                  </a>
                  <a class="social__item text-white border-light" href="https://wa.me/5511951294285?text=Ol%C3%A1,%20gostaria%20de%20um%20or%C3%A7amento">
                    <i class="fa fa-whatsapp"></i>
                  </a>
                </div>
              </div>
              <div class="col-lg-auto col-sm-4">
                <h4 class="text-xl fw-500 text-white">
                  Links
                </h4>

                <div class="footer__content text-base text-white mt-16 sm:mt-12">
                  <div><a data-barba href="blog-1.html" class="button -underline">Blog</a></div>
                  <div><a data-barba href="about-1.html" class="button -underline mt-4">Abous Us</a></div>
                  <div><a data-barba href="#" class="button -underline mt-4">Shop</a></div>
                  <div><a data-barba href="contact-1.html" class="button -underline mt-4">Contacts</a></div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <h4 class="text-xl fw-500 text-white">
                  Nossa localização
                </h4>
                <div class="footer__content text-base text-white mt-16 sm:mt-12" >
                  <p style="font-size: 15px;">Rua Oswaldo Collino, 294 - Pres. Altino<br/>Osasco - SP, 02675-031</p>
                </div>
                <div class="footer__content text-base text-white mt-16 sm:mt-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.1797202209277!2d-46.771877285023!3d-23.526037584700415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ceff19abba19cf%3A0x11ad321b06b48cbc!2sRua%20Oswaldo%20Collino%2C%20294%20-%20Pres.%20Altino%2C%20Osasco%20-%20SP%2C%2002675-031!5e0!3m2!1spt-BR!2sbr!4v1621641103268!5m2!1spt-BR!2sbr" width="100%" height="220" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
                

                
              </div>
            </div>
            <!-- row end -->
          </div>


          <div class="footer__bottom">
            <!-- row start -->
            <div class="row">
              <div class="col">
                <div class="footer__copyright">
                  <p class="text-white">
                    @ 2021 FishBone. Todos os direitos reservados.
                  </p>
                </div>
              </div>
            </div>
            <!-- row end -->
          </div>

        </div>
        <!-- container end -->
      </footer>
      <!-- footer end -->
</main>

    </div>
    <!-- barba container end -->

    <!-- JavaScript -->
    <script src="<?php bloginfo('template_directory');?>/assets/js/vendors.js"></script>
    <script src="<?php bloginfo('template_directory');?>/assets/js/main.js"></script>
    <?php wp_footer(); ?>
</body>

</html>